module.exports = (app) => {
  // const customers = require("../controllers/customer.controller");
  const user = require("../controllers/register.controller.js");
  const authenticateController = require("../controllers/authenticate-controller.js");
  const country = require("../controllers/country.controller.js");
  const city = require("../controllers/city.controller.js");
  const job = require("../controllers/job.controller.js");
  const publisher = require("../controllers/publisher.controller.js");
  const userfunctions = require("../controllers/userfunctions.controller.js");
  const writer = require("../controllers/writer.controller.js");
  const publishercontract = require("../controllers/publisher-contract.controller.js");
  const article = require("../controllers/article.controller.js");
  const publish = require("../controllers/article.controller.js");
  const contract = require("../controllers/contract_step1.controller.js");
  const viewcontracts = require("../controllers/contract-view.controller.js");
  const addkeywords = require("../controllers/keywordadd.controller.js");
  const contract_enabled = require("../controllers/contract_enabled.controller.js");
  const bid_view_writer = require("../controllers/writer-accepted-bid.controller.js");
  const article_writer_details = require("../controllers/writer_article.controller.js");
  const paymentMethod = require("../controllers/payment_details.js");

  const admin = require("../controllers/admin.controller");

  app.post("/country", country.findAll);
  app.post("/city/by", city.findbyID);
  app.post("/user/register", user.register);
  app.post("/api/authenticate", authenticateController.authenticate);
  app.post("/api/job/post", job.create);
  app.post("/api/job/all", job.findAll);
  app.post("/api/job/userid", job.findbyID);
  app.post("/country/add", country.create);
  app.post("/city/add", city.create);
  app.post("/jobs/publisher", publisher.findbyID);
  app.post("/update/publisher/job/status", publisher.updatejobStatusPub);
  app.post("/update/publisher/jobupdate", publisher.updatejob);
  app.post("/update/publisher/deletejob", publisher.deletedjob);
  app.post("/writer/placebid", writer.placeBid);
  app.post("/user/get/list", userfunctions.findAll);
  app.post("/user/get/by", userfunctions.findbyID);
  app.post("/job/get/findbyIDandJobid", job.findbyIDandJobid);
  app.post("/job/get/for/writer", job.findbyIDandJobidWriter);
  app.post("/job/get/bid", publisher.findbidbyjobid);
  app.post("/job/get/bid/for/writer", publisher.findbidbyjobidwriter);
  app.post(
    "/job/get/bid/for/writer/byjobid",
    publisher.findbidbyjobidwriterjobid
  );
  app.post("/job/get/bid/update/publisher", publisher.updatejobbid);
  app.post("/article/category/create", article.createCategory);
  app.post("/article/category/list", article.findAll);
  app.post("/article/publish/request", publish.articlePublisher);
  app.post(
    "/get/bids/for/publisher/contract",
    publishercontract.findbidsbystatususerid
  );
  app.post(
    "/get/bids/for/publisher/bybidid",
    publishercontract.findbidsbystatususeridandjob_bid_id
  );
  app.post("/contract/step1/publisher", contract.contract_step1);
  app.post("/contract/contract/details", contract.findAll);
  app.post("/article/publisher/details", article.getDetail);
  app.post("/article/publisher/details/update", article.updateDetail);
  app.post("/article/publisher/update/status", article.UpdateArticle_status);
  app.post(
    "/article/publisher/update/status/enable",
    article.UpdateArticle_status_enable
  );
  app.post("/article/publisher/viewcontract", viewcontracts.contract_view_user);
  app.post("/article/publisher/addkeywords", addkeywords.contract_step1);
  app.post(
    "/article/publisher/enable_contract",
    contract_enabled.contract_enable_user
  );
  app.post(
    "/article/publisher/view_keywords/byarticle",
    addkeywords.viewkeywords_articles
  );
  app.post(
    "/article/publisher/delete_keywords/byarticle",
    addkeywords.deletekeywords_articles
  );
  app.post(
    "/article/writer/view-bid/accepted",
    bid_view_writer.findbidsbystatususerid
  );
  app.post(
    "/article/writer/view-contract/open",
    bid_view_writer.contract_view_user
  );
  app.post(
    "/article/writer/accept-contract",
    bid_view_writer.contract_accept_user
  );
  app.post(
    "/article/writer/getall/available",
    article_writer_details.getDetail
  );
  app.post("/article/writer/write_article", article_writer_details.write);
  app.post(
    "/article/writer/write_getarticle",
    article_writer_details.getDetailarticle
  );
  app.post(
    "/article/publisher/update_article",
    publishercontract.updateArticleDetails
  );

  app.post("/add/user/paymentMethod", paymentMethod.addPayment);
  app.post("/add/user/paymentMethod/get", paymentMethod.getPaymentDetail);

  app.post("/admin/get/all", admin.findAll);
  app.post("/admin/update", admin.UpdateUser);
};
