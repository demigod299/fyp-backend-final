const sql = require("./db.js");

const Article = function (user) {};

Article.contract_step1 = (newUser, articledetails_get, keywords, result) => {
  query = sql.query(
    "SELECT * FROM `job_bidding` WHERE  `job_bidding_id`=?",
    articledetails_get.bid_id,
    (err, res) => {
      console.log(query.sql);
      if (err) throw err;
      if (res.length > 0) {
        query2 = sql.query(
          "INSERT INTO `Keywords`(`keyword_name`, `keyword_length`, `keyword_type`, `keyword_deleted`, `keyword_enabled`, `job_bidding_job_bidding_id`, `article_details_publisher_article_details_publisher_id`) VALUES ?",
          [newUser],
          (err, res) => {
            console.log(query2.sql);

            if (err) {
              console.log("error: ", err);
              result(err, null);
              return;
            }

            result(null, { res });
          }
        );
      } else {
        result("Cannot Place Articles");
      }
    }
  );
};

Article.viewkeywords_articles = (newUser, result) => {
  console.log(newUser);

  query = sql.query(
    "SELECT * FROM Keywords INNER JOIN article_details_publisher ON Keywords.article_details_publisher_article_details_publisher_id=article_details_publisher.article_details_publisher_id INNER JOIN article_category ON article_category.article_category_id=article_details_publisher.article_details_publisher_category_id WHERE Keywords.article_details_publisher_article_details_publisher_id=? AND keyword_deleted=0 AND keyword_enabled=1",
    [newUser.article_details_publisher_article_details_publisher_id],
    (err, res) => {
      console.log(query.sql);
      if (err) throw err;
      if (res.length > 0) {
        result(null, { res });
      } else {
        result("Cannot Place Articles");
      }
    }
  );
};

Article.deletekeywords_articles = (newUser, result) => {
  console.log(newUser);

  query = sql.query(
    "UPDATE `Keywords` SET `keyword_deleted`=1,`keyword_enabled`=0 WHERE `keywords_id`= ?",
    newUser.keywords_id,
    (err, res) => {
      console.log(query.sql);
      if (err) throw err;
      if (res.affectedRows > 0) {
        query = sql.query(
          "SELECT * FROM `Keywords` WHERE `article_details_publisher_article_details_publisher_id` = ? AND keyword_deleted=0 AND keyword_enabled=1",
          [newUser.article_details_publisher_article_details_publisher_id],
          (err, res) => {
            if (res.length < 1) {
              query = sql.query(
                "UPDATE `article_details_publisher` SET `article_details_publisher_enabled`=0 WHERE `article_details_publisher_id`=?",
                [
                  newUser.article_details_publisher_article_details_publisher_id,
                ],
                (err, res) => {
                  result("Article Disable");
                }
              );
            } else {
              result(null, { res });
            }
          }
        );
      } else {
        result("Cannot Delete Article");
      }
    }
  );
};

module.exports = Article;
