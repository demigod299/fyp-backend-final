const sql = require("./db.js");

// constructor
const Country = function(country) {
  this.country_name = country.name;
  this.country_code = country.code;  
  this.country_enabled=1;
  this.country_deleted=0;
};





Country.getAll = result => {
  sql.query("SELECT `country_id`, `country_name` FROM `country`", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("Country: ", res);
    result(null, {res});
  }
  
  );
};



Country.create = (newUser, result) => {
sql.query("SELECT * FROM country WHERE country_name= ? OR country_code=?",[newUser.country_name,newUser.country_code], (err, res) =>{
  console.log("Here!!",newUser)
  if (err) throw err;
  console.log(res);

  if(res.length==0){  
    sql.query("INSERT INTO country SET ?", newUser, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      console.log("created User: ", { id: res.insertId, ...newUser });
      result(null, { id: res.insertId, ...newUser });
    });
  }else{
    result("Country Already Exist")
  }
});


};


module.exports = Country;