const sql = require("./db.js");

// constructor
const Users = function (user) {
  this.id = user.id;
};

Users.addPayment = (newUser, result) => {
  sql.query(
    "INSERT INTO `payment-detail`(`payment_method`, `payment_account`, `users_user_id`) VALUES (?,?,?)",
    [newUser.paymentMethod, newUser.accountNumber, newUser.userId],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      console.log("users: ", res);
      result(null, { res });
    }
  );
};

Users.getPaymentDetail = (newUser, result) => {
  console.log(newUser);
  var query = sql.query(
    "SELECT * FROM `payment-detail` WHERE `users_user_id` = ? ORDER BY `payment-detail_id` DESC",
    [newUser.userId],
    (err, res) => {
      console.log(query.sql);
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      console.log("User: ", res);
      result(null, { res });
    }
  );
};

module.exports = Users;
