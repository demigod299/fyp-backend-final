const sql = require("./db.js");





const User = function(user) {
    this.job_posted_title = user.job_title;
    this.job_posted_description = user.job_description;
    this.job_posted_price_job = user.price_job;
    this.job_posted_words_job=user.words_job;
    this.job_posted_number_articles=user.number_articles;
    this.job_posted_days_to_finish=user.days_to_finish;  
    this.posted_by_users_id=user.user_id;  
    this.job_posted_job_type=user.job_type;
    this.job_posted_job_pub_status=user.job_pub_status;
    this.job_posted_job_writer_status="Not Bidded";
    this.job_posted_enabled=1;
    this.job_posted_deleted=0;
  };

  User.create = (newUser, result) => {
    console.log(newUser)
    sql.query("SELECT `user_type` FROM users WHERE user_id=?",[newUser.posted_by_users_id], (err, res) =>{
      console.log(res[0].user_type)
      if (err) throw err;
      console.log(res);
      //You will get an array. if no users found it will return.
  
      if(res[0].user_type=="publisher" || res[0].user_type=="admin"){  
        sql.query("INSERT INTO job_posted SET ?", newUser, (err, res) => {
          if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
          }
      
          console.log("Job Posted: ", { id: res.insertId, ...newUser });
          result(null, { id: res.insertId, ...newUser });
        });
      }else{
        result("Not Authorized to Post Job")
      }
    });

};

User.getAll = result => {
  sql.query("SELECT `job_id`, `posted_by_users_id`, `job_posted_title`, `job_posted_description`, `job_posted_price_job`, `job_posted_words_job`, `job_posted_number_articles`, `job_posted_job_type`, `job_posted_days_to_finish`, `job_posted_job_writer_status` FROM `job_posted` WHERE `job_posted_deleted`=0 AND `job_posted_enabled`=1 ORDER BY `job_id` DESC", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("Jobs: ", res);
    result(null, {res});
  }
  
  );
};


User.findbyID = (newUser, result) => {
console.log(newUser)
sql.query("SELECT `job_id`,`job_posted_title`, `job_posted_description`, `job_posted_price_job`, `job_posted_words_job`, `job_posted_number_articles`, `job_posted_job_type`, `job_posted_days_to_finish`,  `job_posted_job_pub_status`, `job_posted_enabled` FROM `job_posted` WHERE `job_posted_deleted`=0 AND `posted_by_users_id`=?", [newUser.user_id], (err, res) => {
  if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("City: ", res);
    result(null, {res});
  }

);
};

User.findbyIDandJobid = (userjobId, result) => {
  console.log(userjobId)
  var query = sql.query("SELECT `job_posted_title`, `job_posted_description`, `job_posted_price_job`, `job_posted_words_job`, `job_posted_number_articles`, `job_posted_job_type`, `job_posted_days_to_finish`,  `job_posted_job_pub_status`, `job_posted_enabled` FROM `job_posted` WHERE `job_posted_deleted`=0 AND `posted_by_users_id`=? AND `job_id`=?", [userjobId.user_id,userjobId.job_id], (err, res) => {
   console.log(query.sql)
    if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      if(res.length>0){
        console.log("Job: ", res);
        result(null, {res});
      }
      else{
        console.log("Here");

        result("Not Found")
            }
     
    }
  
  );
  };
  
  User.findbyIDandJobidWriter = (userjobId, result) => {
    console.log(userjobId)
    var query = sql.query("SELECT `job_posted_title`, `job_posted_description`, `job_posted_price_job`, `job_posted_words_job`, `job_posted_number_articles`, `job_posted_job_type`, `job_posted_days_to_finish`,  `job_posted_job_pub_status`, `job_posted_enabled` FROM `job_posted` WHERE `job_posted_deleted`=0 AND `job_id`=?", [userjobId.job_id], (err, res) => {
     console.log(query.sql)
      if (err) {
          console.log("error: ", err);
          result(null, err);
          return;
        }
        if(res.length>0){
          console.log("Job: ", res);
          result(null, {res});
        }
        else{
          console.log("Here");
  
          result("Not Found")
              }
       
      }
    
    );
};

module.exports = User;