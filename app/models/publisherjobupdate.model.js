const sql = require("./db.js");





const Job = function(user) {
    this.job_posted_title = user.job_title;
    this.job_posted_description = user.job_description;
    this.job_posted_price_job = user.price_job;
    this.job_posted_words_job=user.words_job;
    this.job_posted_number_articles=user.number_articles;
    this.job_posted_days_to_finish=user.days_to_finish;  
    this.posted_by_users_id=user.user_id;  
    this.job_posted_job_type=user.job_type;
    this.job_posted_job_pub_status=user.job_posted_job_pub_status;
  };

  Job.updatejob = (newUser, userdata, result ) => {
    console.log(newUser)
    console.log(userdata)
    var query = sql.query("SELECT * FROM job_posted WHERE posted_by_users_id=? AND job_id=?",[newUser.posted_by_users_id,userdata.job_id], (err, res) =>{
      console.log(query.sql)
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;

      }
      
      if(res.length>0){  
        var query2 = sql.query("UPDATE `job_posted` SET ? WHERE job_id=?", [newUser,userdata.job_id], (err, res) => {
            console.log(query2.sql)
          if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
          }
      
          console.log("Job Posted: ", { id: res.insertId, ...newUser });
          result(null, { id: res.insertId, ...newUser });
        });
      }else{
        result("not updated")
      }
    });

};



module.exports = Job;