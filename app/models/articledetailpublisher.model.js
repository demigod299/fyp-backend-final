const sql = require("./db.js");

const Detail = function(user) {

  };

Detail.getDetail  = (newUser, result) => {
  if (newUser.job_bidding_job_bidding_id!=null && newUser.job_posted_job_id!=null && newUser.article_details_publisher_enabled!=null){
      
      query = "Select * from  article_details_publisher INNER JOIN article_category on article_category.article_category_id=article_details_publisher.article_details_publisher_category_id WHERE job_bidding_job_bidding_id=? AND job_posted_job_id=? AND article_details_publisher_enabled=?"
      value = [newUser.job_bidding_job_bidding_id,newUser.job_posted_job_id,newUser.article_details_publisher_enabled]
     
    }
    else if(newUser.article_details_publisher_enabled == null && newUser.job_bidding_job_bidding_id!=null && newUser.job_posted_job_id!=null){
      query = "Select * from  article_details_publisher INNER JOIN article_category on article_category.article_category_id=article_details_publisher.article_details_publisher_category_id WHERE job_bidding_job_bidding_id=? AND job_posted_job_id=?"
      value = [newUser.job_bidding_job_bidding_id,newUser.job_posted_job_id]
    }
      
  else{
      query = "SELECT * from article_details_publisher INNER JOIN article_category on article_category.article_category_id=article_details_publisher.article_details_publisher_category_id WHERE article_details_publisher_id=?"
      value = [newUser.article_details_publisher_id]
  }
    var query2 = sql.query(query, value, (err, res) => {
        console.log(value)
        console.log(query2.sql)
        if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      if(res.length>0){
        
        console.log("Jobs: ", res);
        result(null, {res});
      }
      else{
        result("No Data Found");
      }
     
    }
    
    );
  };
  


  Detail.UpdateArticle_status_enable =  (newUser, result)  => {
   var query= sql.query("SELECT * FROM `Keywords` WHERE `article_details_publisher_article_details_publisher_id`=? AND keyword_deleted=0 AND keyword_enabled=1",[newUser.article_details_publisher_id], (err, res) => {
     console.log(query.sql)
    if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      console.log(res.length);
      if(res.length>0){
        sql.query("UPDATE `article_details_publisher` SET `article_details_publisher_enabled`=? WHERE `article_details_publisher_id`=?",[newUser.article_details_publisher_enabled,newUser.article_details_publisher_id], (err, res) => {
          if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
          }
      
          console.log("Updated Articles: ", res);
          result(null, {res});
        }
        
        );
      }else{
        result("Cannot Enable Articles")
      }
     
    }
    );
  };












  Detail.UpdateArticle_status =  (newUser, result)  => {
    sql.query("UPDATE `article_details_publisher` SET `article_details_publisher_enabled`=? WHERE `article_details_publisher_id`=?",[newUser.article_details_publisher_enabled,newUser.article_details_publisher_id], (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      console.log("Updated Articles: ", res);
      result(null, {res});
    }
    
    );
  };


  
  

  module.exports = Detail;