const sql = require("./db.js");
var Cryptr = require("cryptr");
cryptr = new Cryptr("myTotalySecretKey");

const User = function (user) {
  this.user_email = user.email;
  this.user_password = user.password;
};

User.authenticate = (newUser, result) => {
  console.log(newUser);
  var query = sql.query(
    "SELECT * FROM users WHERE user_email= ? OR user_username=? AND user_deleted=0 AND user_enable=1",
    [newUser.user_email, newUser.user_email],
    (err, res) => {
      console.log(query.sql);
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      } else if (res.length == 0) {
        result("No Email/Username Found");
      } else if (res.length > 0) {
        console.log(res[0].user_password);
        var decryptedString = cryptr.decrypt(res[0].user_password);
        console.log(res[0].user_id);
        if (newUser.user_password == decryptedString) {
          console.log(res);
          result(null, { id: res[0].user_id, type: res[0].user_type });
        } else {
          result("Not Authenticated");
        }
      } else result("Not OK");
    }
  );
};

module.exports = User;
