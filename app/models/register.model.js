const sql = require("./db.js");





const User = function(user) {
    this.user_email = user.email;
    this.user_username = user.username;
    this.user_password = user.password;
    this.user_name=user.name;
    this.user_phone=user.phone;
    this.city_city_id=user.city;  
    this.user_type=user.type;
    this.user_enable=1;
    this.user_deleted=0;
  
  };

  User.registeruser = (newUser, result) => {
  sql.query("SELECT * FROM users WHERE user_email= ? OR user_username=?",[newUser.user_email,newUser.user_username], (err, res) =>{
    if (err) throw err;
    console.log(res);

    if(res.length==0){  
      sql.query("INSERT INTO users SET ?", newUser, (err, res) => {
        if (err) {
          console.log("error: ", err);
          result(err, null);
          return;
        }
    
        console.log("created User: ", { id: res.insertId, ...newUser });
        result(null, { id: res.insertId, ...newUser });
      });
    }else{
      result("Email or Username Already")
    }
  });


};

module.exports = User;