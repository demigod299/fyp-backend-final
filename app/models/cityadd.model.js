const sql = require("./db.js");



const CityAdd = function(user) {
  this.city_code= user.code,
  this.city_name= user.name,
  this.country_country_id=user.country_id,
  this.city_enabled=1,
  this.city_deleted=0
}



CityAdd.create = (newUser, result) => {
  sql.query("SELECT * FROM `city` WHERE  `country_country_id`=? AND `city_name`=? OR `country_country_id`=? AND `city_code`=?",[newUser.country_country_id,newUser.city_name,newUser.country_country_id,newUser.city_code], (err, res) =>{
    console.log("Here!!",newUser)
    if (err) throw err;
    console.log(res);
  
    if(res.length==0){  
      sql.query("INSERT INTO city SET ?", newUser, (err, res) => {
        if (err) {
          console.log("error: ", err);
          result(err, null);
          return;
        }
    
        console.log("created User: ", { id: res.insertId, ...newUser });
        result(null, { id: res.insertId, ...newUser });
      });
    }else{
      result("City Already Exist")
    }
  });
  
  
  };

module.exports = CityAdd;