const sql = require("./db.js");

const Article = function (user) {};

Article.contract_step1 = (newUser, articledetails_get, result) => {
  console.log(newUser);
  sql.query(
    "Select * from  contract_details_steps WHERE `job_bidding_job_bidding_id`=? AND `contract_details_steps_status` ='Articles Placed'",
    articledetails_get.job_bidding_job_bidding_id,
    (err, res) => {
      console.log("Here!!", newUser);
      if (err) throw err;
      console.log(res);

      if (res.length == 0) {
        sql.query(
          "INSERT INTO article_details_publisher (`job_posted_job_id`, `article_details_publisher_article_suggested_title`, `article_details_publisher_seo_optimized`, `article_details_publisher_article_wordcount`, `article_details_article_description`, `article_details_publisher_headings`, `article_details_publisher_article_source`, `article_details_publisher_images_number`, `article_details_publisher_plagirism_percentage`, `article_details_publisher_category_id`, `article_details_publisher_article_status`, `article_details_publisher_enabled`, `article_details_publisher_deleted`,`article_details_publisher_status_article`, `job_bidding_job_bidding_id`) VALUES ?",
          [newUser],
          (err, res) => {
            if (err) throw err;
            console.log(res);

            if (res.affectedRows == newUser.length) {
              sql.query(
                "INSERT INTO contract_details_steps SET `contract_details_steps_status`='Articles Placed', `job_bidding_job_bidding_id`=?",
                articledetails_get.job_bidding_job_bidding_id,
                (err, res) => {
                  if (err) {
                    console.log("error: ", err);
                    result(err, null);
                    return;
                  }
                  result(null, { res });
                }
              );
            } else {
              result("Cannot Place Articles");
            }
          }
        );
      } else {
        result("You Have Already Placed Article Details");
      }
    }
  );
};

module.exports = Article;
