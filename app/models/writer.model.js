const sql = require("./db.js");





const Writer = function(user) {
    this.job_bidding_amount=user.job_bidding_amount;
    this.job_bidding_day_finish=user.job_bidding_day_finish;
    this.job_bidding_status=user.job_bidding_status;
    this.job_posted_id=user.job_posted_id;
    this.bid_by_user_id=user.bid_by_user_id;
    this.job_bidding_enabled=1;
    this.job_bidding_deleted=0;
 

  };

  Writer.placeBid = (newUser, result) => {
    console.log(newUser)
    sql.query("SELECT `user_type` FROM users WHERE user_id=?",[newUser.bid_by_user_id], (err, res) =>{
      if (err) throw err;
      console.log(res);
      //You will get an array. if no users found it will return.
  
      if(res[0].user_type=="writer" || res[0].user_type=="admin"){  
        sql.query("SELECT * FROM job_bidding WHERE bid_by_user_id=? AND job_posted_id=? AND job_bidding_Status!='declined'", [newUser.bid_by_user_id, newUser.job_posted_id], (err, res) => {
          if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
          }
          if (res.length==0){
            sql.query("INSERT INTO job_bidding SET ?", newUser, (err, res) => {
              if (err) {
                console.log("error: ", err);
                result(err, null);
                return;

              }
              console.log(res)
              if (res.affectedRows>0){
                console.log(res.affectedRows)
               var query = sql.query("UPDATE job_posted SET job_posted_job_writer_status='Bidded' where job_id=?", [newUser.job_posted_id], (err, res) => {
                console.log(query.sql)  
                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                    return;
                  }
              
                  console.log("Bid Placed: ", { id: res.insertId, ...newUser });
                  result(null, { id: res.insertId, ...newUser });
                });
              }
              else{
                result("Already Bid Placed")
              }
          
          
            });


          }
          else{
            result("Already Bid Placed")
          }
      
         
        });
      }else{
        result("Not Authorized to Place Bid")
      }
    });

};





module.exports = Writer;