const sql = require("./db.js");



const User = function(user) {
    this.job_posted_enabled = user.enabled;
  };

  User.updateStatus = (newUser,datauser, result) => {
  var query= sql.query("SELECT * FROM job_posted WHERE job_id= ? AND posted_by_users_id=?",[newUser.job_id,newUser.userid], (err, res) =>{
      console.log(query.sql)
    if (err) throw err;
    console.log(res);

    if(res.length>0){  
      sql.query("UPDATE `job_posted` SET `job_posted_enabled`=? WHERE `job_id`=?", [datauser.job_posted_enabled,newUser.job_id], (err, res) => {
        if (err) {
          console.log("error: ", err);
          result(err, null);
          return;
        }
    
        console.log("Status Updated", { id: res.insertId, ...newUser });
        result(null, { id: res.insertId, ...newUser });
      });
    }else{
      result("No Job Found")
    }
  });


};

module.exports = User;