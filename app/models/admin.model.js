const sql = require("./db.js");

const Article = function (user) {
  this.article_category_name = user.article_category_name;
  this.article_category_enabled = 1;
  this.article_category_disabled = 0;
};

Article.getAll = (result) => {
  sql.query(
    "SELECT * FROM `users` WHERE `user_type` != 'admin'",
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      console.log("Category: ", res);
      result(null, { res });
    }
  );
};

Article.UpdateUser = (newUser, result) => {
  sql.query(
    "UPDATE `users` SET `user_enable`=? WHERE `user_id` = ?",
    [newUser.status, newUser.id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      result(null, { res });
    }
  );
};

module.exports = Article;
