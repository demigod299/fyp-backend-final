const sql = require("./db.js");

const JobBid = function(user) {
    
  };

JobBid.findbidbyjobid = (userjobId, result) => {
  console.log(userjobId)
  var query = sql.query("SELECT * FROM job_bidding INNER JOIN (SELECT user_id,user_username,user_name from users) as c1 on job_bidding.bid_by_user_id=c1.user_id WHERE job_bidding.job_posted_id=? AND job_bidding.job_bidding_Status=? AND`job_bidding_deleted`=0 AND `job_bidding_enabled`=1 ORDER BY job_bidding.job_bidding_id DESC", [userjobId.job_id,userjobId.status], (err, res) => {
   console.log(query.sql)
    if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      if(res.length>0){
        
          console.log(res.length)
          var dataArray =[]
          for(i=0;i<res.length;i++){
            dataArray[i]={              
                "job_bidding_id":res[i].job_bidding_id,
                "job_bidding_amount":res[i].job_bidding_amount,
                "job_bidding_day_finish":res[i].job_bidding_day_finish,
                "job_bidding_Status":res[i].job_bidding_Status,
                "bid_by_user_id":res[i].bid_by_user_id,
                "user_id":res[i].user_id,
                "user_username":res[i].user_username,
                "user_name":res[i].user_name,                
              }
          }
         
        console.log("Job: ", dataArray);
        result(null, {dataArray});
      }
      else{
        console.log("Here");

        result("Not Found")
            }
     
    }
  
  );
  };
  

  JobBid.findbidbyjobidwriter = (userjobId, result) => {
    console.log(userjobId)
    var query = sql.query("SELECT * FROM job_bidding INNER JOIN (SELECT user_id,user_username,user_name from users) as c1 on job_bidding.bid_by_user_id=c1.user_id WHERE job_bidding.job_posted_id=? AND `job_bidding_deleted`=0 AND `job_bidding_enabled`=1 ORDER BY job_bidding.job_bidding_id DESC", [userjobId.job_id], (err, res) => {
     console.log(query.sql)
      if (err) {
          console.log("error: ", err);
          result(null, err);
          return;
        }
        if(res.length>0){
          
            console.log(res.length)
            var dataArray =[]
            for(i=0;i<res.length;i++){
              dataArray[i]={              
              
                  "job_bidding_Status":res[i].job_bidding_Status,
                  "bid_by_user_id":res[i].bid_by_user_id,
                  "user_id":res[i].user_id,
                  "user_username":res[i].user_username,
                  "user_name":res[i].user_name,                
                }
            }
           
          console.log("Job: ", dataArray);
          result(null, {dataArray});
        }
        else{
          console.log("Here");
  
          result("Not Found")
              }
       
      }
    
    );
    };


    JobBid.findbidbyjobidwriterjobid = (userjobId, result) => {
      console.log(userjobId)
      var query = sql.query("SELECT * FROM job_bidding INNER JOIN (SELECT user_id,user_username,user_name from users) as c1 on job_bidding.bid_by_user_id=c1.user_id WHERE job_bidding.job_posted_id=? AND job_bidding.bid_by_user_id=? AND`job_bidding_deleted`=0 AND `job_bidding_enabled`=1  ORDER BY job_bidding.job_bidding_id DESC", [userjobId.job_id,userjobId.user_id], (err, res) => {
       console.log(query.sql)
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
          }
          if(res.length>0){
            
              console.log(res.length)
              var dataArray =[]
              for(i=0;i<res.length;i++){
                dataArray[i]={             
                  "job_bidding_id":res[i].job_bidding_id,
                  "job_bidding_amount":res[i].job_bidding_amount,
                  "job_bidding_day_finish":res[i].job_bidding_day_finish,
                 
                    "job_bidding_Status":res[i].job_bidding_Status,
                    "bid_by_user_id":res[i].bid_by_user_id,
                    "user_id":res[i].user_id,
                    "user_username":res[i].user_username,
                    "user_name":res[i].user_name,                
                  }
              }
             
            console.log("Job: ", dataArray);
            result(null, {dataArray});
          }
          else{
            console.log("Here");
    
            result("Not Found")
                }
         
        }
      
      );
      };

module.exports = JobBid;