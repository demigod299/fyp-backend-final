const sql = require("./db.js");

const Detail = function(user) {

  };

Detail.getDetail  = (newUser, result) => {
 
    var query2 = sql.query("SELECT * FROM article_details_publisher INNER JOIN job_bidding ON article_details_publisher.job_bidding_job_bidding_id=job_bidding.job_bidding_id INNER JOIN article_category ON article_details_publisher.article_details_publisher_category_id=article_category.article_category_id WHERE job_bidding.bid_by_user_id=? AND article_details_publisher.article_details_publisher_status_article=? AND article_details_publisher.article_details_publisher_enabled=1", [newUser.user_id,newUser.user_status], (err, res) => {
       console.log(query2.sql)
        if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      if(res.length>0){
        
        console.log("Jobs: ", res);
        result(null, {res});
      }
      else{
        result("No Data Found");
      }
     
    }
    
    );
  };

  Detail.getDetailarticle  = (newUser, result) => {
 
    var query2 = sql.query("SELECT * FROM `article_details_writer` WHERE `article_details_publisher_article_details_publisher_id`=?", [newUser.article_details_publisher_article_details_publisher_id], (err, res) => {
       console.log(query2.sql)
        if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      if(res.length>0){
        
        console.log("Jobs: ", res);
        result(null, {res});
      }
      else{
        result("No Data Found");
      }
     
    }
    
    );
  };
  

  Detail.write  = (newUser,newuser2,userdata, result) => {
 
    var query2 = sql.query("SELECT * FROM article_details_writer WHERE article_details_publisher_article_details_publisher_id=?", [newUser.article_details_publisher_article_details_publisher_id], (err, res) => {
        console.log(query2.sql)
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      console.log(res.length)
      if(res.length<1){
        var query3 = sql.query("INSERT INTO `article_details_writer`(`job_posted_id`, `article_details_publisher_article_details_publisher_id`, `article_details_title`, `article_details_content`, `article_details_status`, `article_details_deleted`, `article_details_enabled`,`job_bidding_job_bidding_id`) VALUES (?,?,?,?,?,?,?,?)", [newUser.job_posted_id,newUser.article_details_publisher_article_details_publisher_id,newUser.article_details_title,newUser.article_details_content,newUser.article_details_status,newUser.article_details_deleted,newUser.article_details_enabled,newUser.job_bidding_job_bidding_id], (err, res) => {
          console.log(query3.sql)

          if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
          }
          result(null, {res});

        
        
        })


      }
      else{
        var query4 =  sql.query("UPDATE `article_details_writer` SET `article_details_title`=?,`article_details_content`=?,`article_details_status`=?,`article_details_enabled`=1 WHERE `article_details_publisher_article_details_publisher_id`=?", [newuser2.article_details_title,newuser2.article_details_content,newuser2.article_details_status,newUser.article_details_publisher_article_details_publisher_id], (err, res) => {
          console.log(query4.sql)

          if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
          }
          console.log(res.affectedRows)
          console.log(newuser2.article_details_status)
          console.log(userdata.article_details_publisher_article_details_publisher_id)

          if(res.affectedRows>0){
            if(newuser2.article_details_status=="Submitted"){
            sql.query("UPDATE `article_details_publisher` SET `article_details_publisher_status_article`='Submitted By Writer' WHERE `article_details_publisher_id`=?", [userdata.article_details_publisher_article_details_publisher_id], (err, res) => {
              if (err) {
                console.log("error: ", err);
                result(null, err);
                return;}

                result(null, {res});

              
              })

            }else{
              result(null, {res});
            }

          }else{
            result("No Data Found");

          }

        
        
        })
      }
     
    }
    
    );
  };
  

  

  module.exports = Detail;