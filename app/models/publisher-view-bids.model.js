const sql = require("./db.js");

const JobBidView = function (user) {};

JobBidView.findbidsbystatususerid = (userjobId, result) => {
  console.log(userjobId);
  var query = sql.query(
    "SELECT * FROM job_posted INNER JOIN job_bidding on job_posted.job_id=job_bidding.job_posted_id INNER JOIN users on users.user_id=job_bidding.bid_by_user_id WHERE job_posted.posted_by_users_id=? AND job_bidding.job_bidding_Status=?",
    [userjobId.user_id, userjobId.status],
    (err, res) => {
      console.log(query.sql);
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      if (res.length > 0) {
        console.log(res.length);
        var dataArray = [];
        for (i = 0; i < res.length; i++) {
          dataArray[i] = {
            job_id: res[i].job_id,
            posted_by_users_id: res[i].posted_by_users_id,
            job_posted_title: res[i].job_posted_title,
            job_posted_description: res[i].job_posted_description,
            job_posted_price_job: res[i].job_posted_price_job,
            job_posted_words_job: res[i].job_posted_words_job,
            job_posted_number_articles: res[i].job_posted_number_articles,
            job_posted_job_type: res[i].job_posted_job_type,
            job_posted_days_to_finish: res[i].job_posted_days_to_finish,
            job_posted_job_pub_status: res[i].job_posted_job_pub_status,
            bid_details: {
              job_bidding_id: res[i].job_bidding_id,
              job_bidding_day_finish: res[i].job_bidding_day_finish,
              job_bidding_amount: res[i].job_bidding_amount,
              job_bidding_Status: res[i].job_bidding_Status,
              bid_by_user_id: res[i].bid_by_user_id,
              bid_by_user_name: res[i].user_username,
              bid_by_user_username: res[i].user_name,
            },
          };
        }

        console.log("Job: ", dataArray);
        result(null, { dataArray });
      } else {
        console.log("Here");

        result("Not Found");
      }
    }
  );
};

JobBidView.findbidsbystatususeridandjob_bid_id = (userjobId, result) => {
  console.log(userjobId);
  var query = sql.query(
    "SELECT * FROM job_posted INNER JOIN job_bidding on job_posted.job_id=job_bidding.job_posted_id INNER JOIN users on users.user_id=job_bidding.bid_by_user_id WHERE job_posted.posted_by_users_id=? AND job_bidding.job_bidding_id=? AND job_bidding.job_bidding_Status=?",
    [userjobId.user_id, userjobId.bid_job_id, userjobId.status],
    (err, res) => {
      console.log(query.sql);
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      if (res.length > 0) {
        console.log(res.length);
        var dataArray = [];
        for (i = 0; i < res.length; i++) {
          dataArray[i] = {
            job_id: res[i].job_id,
            posted_by_users_id: res[i].posted_by_users_id,
            job_posted_title: res[i].job_posted_title,
            job_posted_description: res[i].job_posted_description,
            job_posted_price_job: res[i].job_posted_price_job,
            job_posted_words_job: res[i].job_posted_words_job,
            job_posted_number_articles: res[i].job_posted_number_articles,
            job_posted_job_type: res[i].job_posted_job_type,
            job_posted_days_to_finish: res[i].job_posted_days_to_finish,
            job_posted_job_pub_status: res[i].job_posted_job_pub_status,
            bid_details: {
              job_bidding_day_finish: res[i].job_bidding_day_finish,
              job_bidding_amount: res[i].job_bidding_amount,
              job_bidding_Status: res[i].job_bidding_Status,
              bid_by_user_id: res[i].bid_by_user_id,
              bid_by_user_name: res[i].user_username,
              bid_by_user_username: res[i].user_name,
            },
          };
        }

        console.log("Job: ", dataArray);
        result(null, { dataArray });
      } else {
        console.log("Here");

        result("Not Found");
      }
    }
  );
};

JobBidView.updateArticleDetails = (newUser, result) => {
  var query2 = sql.query(
    "UPDATE `article_details_writer` SET `article_details_status`=? WHERE `article_details_id`=?",
    [newUser.article_status, newUser.article_id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      if (res.affectedRows == 0) {
        result("Not Found");
        return;
      }

      console.log("Updated Article: ", res);
      result(null, { res });
    }
  );
};

module.exports = JobBidView;
