const Writer = require("../models/writer.model.js");
var Cryptr = require('cryptr');

exports.placeBid = (req, res) => {
    console.log(req.body)
    if (!req.body) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
    }
  
    const user = new Writer({
      job_bidding_amount: req.body.job_bidding_amount,
      job_bidding_day_finish: req.body.job_bidding_day_finish,
      job_bidding_status: req.body.job_bidding_status,
      job_posted_id: req.body.job_posted_id,
      bid_by_user_id: req.body.bid_by_user_id,

    });
  
    // Save Customer in the database
    Writer.placeBid(user, (err, data) => {
      console.log(user)
      if (err){
        if(err == "Already Bid Placed"){
        res.status(500).send({
          message:
            err.message || "Already Bid Placed."
        });
      }
      else{
        res.status(500).send({
          message:
            err.message || "You Are Not Allowed to Bid."
        });
      }
        } 
        
        else {
          res.status(200).send(
            {                
            status:"ok",
            message: "Bid Placed",    
            data,    
            }
        )      
        }
    });
  };
