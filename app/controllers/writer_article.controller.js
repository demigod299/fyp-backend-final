
const Detail = require("../models/writer_article.model.js");

  
      module.exports.getDetail=function(req,res){
         
          var article_details={
              user_id: req.body.user_id,
              user_status:req.body.user_status,
             
              
          }
          Detail.getDetail(article_details, (err, data) => {
              if (err) {
                  if (err.kind === "not_found") {
                    res.status(404).send({
                      message: `Not Article Details Found`
                    });
                  } else {
                    res.status(500).send({
                      message: "Error retrieving Article Details"
                    });
                  }
                } 
                else{  res.status(200).send(
                  {                
                  status:"ok",
                  message: "Article Details Retreieved Successfully",    
                  data,    
                  });
                }
                      }
            
            );
      }

      module.exports.getDetailarticle=function(req,res){
         
        var article_details={
          article_details_publisher_article_details_publisher_id:req.body.article_details_publisher_article_details_publisher_id,
           
            
        }
        Detail.getDetailarticle(article_details, (err, data) => {
            if (err) {
                if (err.kind === "not_found") {
                  res.status(404).send({
                    message: `Not Article Details Found`
                  });
                } else {
                  res.status(500).send({
                    message: "Error retrieving Article Details"
                  });
                }
              } 
              else{  res.status(200).send(
                {                
                status:"ok",
                message: "Article Details Retreieved Successfully",    
                data,    
                });
              }
                    }
          
          );
    }



      module.exports.write=function(req,res){
         
        var article_details={
          job_posted_id: req.body.job_posted_id,
          article_details_publisher_article_details_publisher_id:req.body.article_details_publisher_article_details_publisher_id,
          article_details_title:req.body.article_details_title,
          article_details_content:req.body.article_details_content,
          article_details_status:req.body.article_details_status,
          article_details_deleted:0,
          article_details_enabled:1,
          job_bidding_job_bidding_id:req.body.job_bidding_job_bidding_id,
           
            
        }
        var newuser={
          article_details_title:req.body.article_details_title,
          article_details_content:req.body.article_details_content,
          article_details_status:req.body.article_details_status,
          article_details_enabled:1,
            
        }
        userdata={
          article_details_publisher_article_details_publisher_id:req.body.article_details_publisher_article_details_publisher_id,

        }
        Detail.write(article_details,newuser,userdata, (err, data) => {
            if (err) {
                if (err.kind === "not_found") {
                  res.status(404).send({
                    message: `Not Article Details Found`
                  });
                } else {
                  res.status(500).send({
                    message: "Error retrieving Article Details"
                  });
                }
              } 
              else{  res.status(200).send(
                {                
                status:"ok",
                message: "Article Details Saved Successfully",    
                data,    
                });
              }
                    }
          
          );
    }
