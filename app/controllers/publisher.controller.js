const Publisher = require("../models/publisherjobs.model.js");
const User = require("../models/publisherupdatejobstatus.model.js");
const Job = require("../models/publisherjobupdate.model.js");
const Del = require("../models/publisherdeletedjob.model.js");
const JobBid = require("../models/publishercheckbid.model.js");
const JobBidupdate = require("../models/publisherupdatejobbid.model.js");




module.exports.findbyID=function(req,res){
    const city = new Publisher({
        id: req.body.id,
    
});
   
Publisher.findbyID(city, (err, data) => {
        if (err) {
            console.log(err)
            if (err === "No Job Found") {
              res.status(404).send({
                message: `Not Jobs found`
              });
            } else {
              res.status(500).send({
                message: "Error retrieving Jobs"
              });
            }
          } else res.send(data);
                }
      
      );
    
}

exports.updatejobStatusPub = (req, res) => {
    if (!req.body) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
    }
  
    const user = new User({
      enabled: req.body.enabled,
      userid: req.body.userid,
      job_id: req.body.job_id,    

    });
    
    var userdata = {
      enabled: req.body.enabled,
      userid: req.body.userid,
      job_id: req.body.job_id,
    }
    // Save Customer in the database
    User.updateStatus(userdata,user, (err, data) => {
      if (err)
      if(err="No Job Found"){
        res.status(500).send({
          message:
            err.message || "No Job Found."
        });
      }
        else{
          res.status(500).send({
            message:
              err.message || "Error Updating Job."
          });
        }
      
        
      else       
      res.status(200).send({
        message:
           "Status Updated."
      });
    });
  };



  exports.updatejob = (req, res) => {
    console.log(req.body)
    if (!req.body) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
    }
  
    const user = new Job({
      job_title: req.body.job_title,
      job_description: req.body.job_description,
      price_job: req.body.price_job,
      words_job: req.body.words_job,
      number_articles: req.body.number_articles,
      days_to_finish: req.body.days_to_finish,
      user_id: req.body.user_id,
      job_type: req.body.job_type,
      job_posted_job_pub_status: req.body.job_posted_job_pub_status,
      

    });
  var userdata={
    job_id: req.body.job_id,
  }

    // Save Customer in the database
    Job.updatejob(user, userdata, (err, data) => {
      console.log(user)
      if (err){
     
          res.status(500).send({
            message:
              err.message || "You Are Not Allowed to Update Job."
          });
        } 
        
        else {
          res.status(200).send(
            {                
            status:"ok",
            message: "Job Updated",    
            data,    
            }
        )      
        }
    });
  };




  exports.deletedjob = (req, res) => {
    console.log(req.body)
    if (!req.body) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
    }
  
    const user = new Del({
      user_id: req.body.user_id,
      
      

    });
  var userdata={
    job_id: req.body.job_id,
  }

    // Save Customer in the database
    Del.deletedjob(user, userdata, (err, data) => {
      console.log(user)
      if (err){
     
          res.status(500).send({
            message:
              err.message || "You Are Not Allowed to Delete a Job."
          });
        } 
        
        else {
          res.status(200).send(
            {                
            status:"ok",
            message: "Job Deleted",    
            data,    
            }
        )      
        }
    });
  };
  


  module.exports.findbidbyjobid=function(req,res){
   

    userjobId={
      "job_id": req.body.job_id,
      "status":req.body.bid_status

    }

    JobBid.findbidbyjobid(userjobId, (err, data) => {
        if (err) {
          console.log(err)
            if (err =="Not Found") {
              res.status(404).send({
                message: `Not found Jobs For This User`
              });
            } else {
              res.status(500).send({
                message: "Error retrieving Jobs"
              });
            }
          } else {
            res.status(200).send(
              {                
              status:"ok",
              message: "Job Posted",    
              data,    
              }
          )      
          }
                }
      
      );
    
}


module.exports.findbidbyjobidwriter=function(req,res){
   

  userjobId={
    "job_id": req.body.job_id,
  }

  JobBid.findbidbyjobidwriter(userjobId, (err, data) => {
      if (err) {
        console.log(err)
          if (err =="Not Found") {
            res.status(404).send({
              message: `Not found Jobs For This User`
            });
          } else {
            res.status(500).send({
              message: "Error retrieving Jobs"
            });
          }
        } else {
          res.status(200).send(
            {                
            status:"ok",
            message: "Job Posted",    
            data,    
            }
        )      
        }
              }
    
    );
  
}


module.exports.findbidbyjobidwriterjobid=function(req,res){
   

  userjobId={
    "job_id": req.body.job_id,
    "user_id":req.body.user_id
  }

  JobBid.findbidbyjobidwriterjobid(userjobId, (err, data) => {
      if (err) {
        console.log(err)
          if (err =="Not Found") {
            res.status(404).send({
              message: `Not found Jobs For This User`
            });
          } else {
            res.status(500).send({
              message: "Error retrieving Jobs"
            });
          }
        } else {
          res.status(200).send(
            {                
            status:"ok",
            message: "Job Posted",    
            data,    
            }
        )      
        }
              }
    
    );
  
}





exports.updatejobbid = (req, res) => {
  console.log(req.body)
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }
  
          var userdata={
              user_id: req.body.user_id,
              job_id: req.body.job_id,
              bidding_id: req.body.bidding_id,
              job_bid_status: req.body.job_bid_status,
          }

  // Save Customer in the database
  JobBidupdate.updatejobbid(userdata, (err, data) => {
    console.log(userdata)
    if (err){
   
        res.status(500).send({
          message:
            err.message || "You Are Not Allowed to Update Bid."
        });
      } 
      
      else {
        res.status(200).send(
          {                
          status:"ok",
          message: "Bid Updated",    
          data,    
          }
      )      
      }
  });
};