const Article = require("../models/articlecategory.model.js");
const Publish = require("../models/articlepublisher.model.js");
const Detail = require("../models/articledetailpublisher.model.js");
const Change = require("../models/articledetailpublisherupdate.model.js");

var Cryptr = require('cryptr');

exports.createCategory = (req, res) => {
    console.log(req.body)
    if (!req.body) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
    }
  
    const user = new Article({

      article_category_name: req.body.article_category_name,

    });
  
    // Save Customer in the database
    Article.createCategory(user, (err, data) => {
      console.log(user)
      if (err){
     
          res.status(500).send({
            message:
              err.message || "You Are Not Allowed to Add Category."
          });
        } 
        
        else {
          res.status(200).send(
            {                
            status:"ok",
            message: "Category Created",    
            data,    
            }
        )      
        }
    });
  };



  
// Retrieve all Customers from the database.
exports.findAll = (req, res) => {


    Article.getAll((err, data) => {
        if (err)
          res.status(500).send({
            message:
              err.message || "Some error occurred while retrieving Country."
          });
        else res.send(data);
      });
    };
  

    exports.articlePublisher = (req, res) => {
        console.log(req.body)
        if (!req.body) {
          res.status(400).send({
            message: "Content can not be empty!"
          });
        }
      
        const user = new Publish({
    
          article_category_name: req.body.article_category_name,
    
        });
      
        // Save Customer in the database
        Publish.articlePublisher(user, (err, data) => {
          console.log(user)
          if (err){
         
              res.status(500).send({
                message:
                  err.message || "You Are Not Allowed to Publish Article."
              });
            } 
            
            else {
              res.status(200).send(
                {                
                status:"ok",
                message: "Category Created",    
                data,    
                }
            )      
            }
        });
      };

      
      
      module.exports.getDetail=function(req,res){
          const city = new Detail({
          
      });
          var article_details={
              article_details_publisher_id: req.body.article_details_publisher_id,
              job_bidding_job_bidding_id: req.body.job_bidding_job_bidding_id,
              article_details_publisher_enabled: req.body.article_details_publisher_enabled,
              job_posted_job_id: req.body.job_posted_job_id,

              
          }
          Detail.getDetail(article_details, (err, data) => {
              if (err) {
                  if (err.kind === "not_found") {
                    res.status(404).send({
                      message: `Not Article Details Found`
                    });
                  } else {
                    res.status(500).send({
                      message: "Error retrieving Article Details"
                    });
                  }
                } 
                else{  res.status(200).send(
                  {                
                  status:"ok",
                  message: "Article Details Retreieved Successfully",    
                  data,    
                  });
                }
                      }
            
            );
      }



      module.exports.UpdateArticle_status=function(req,res){
          const city = new Detail({
          
      });
          var article_details={
            article_details_publisher_id: req.body.article_details_publisher_id,
            article_details_publisher_enabled: req.body.article_details_publisher_enabled,            

              
          }
          Detail.UpdateArticle_status(article_details, (err, data) => {
              if (err) {
                  
                    res.status(500).send({
                      message: "You Need to Add More Than 1 keyword for this Article To Enable It"
                    });
                  
                } 
                else{  res.status(200).send(
                  {                
                  status:"ok",
                  message: "Article Details Retreieved Successfully",    
                  data,    
                  });
                }
                      }
            
            );
      }




      module.exports.UpdateArticle_status_enable=function(req,res){
        const city = new Detail({
        
    });
        var article_details={
          article_details_publisher_id: req.body.article_details_publisher_id,
          article_details_publisher_enabled: req.body.article_details_publisher_enabled,            

            
        }
        Detail.UpdateArticle_status_enable(article_details, (err, data) => {
            if (err) {
                
                  res.status(500).send({
                    message: "You Need to Add More Than 1 keyword for this Article To Enable It"
                  });
                
              } 
              else{  res.status(200).send(
                {                
                status:"ok",
                message: "Article Details Retreieved Successfully",    
                data,    
                });
              }
                    }
          
          );
    }
      exports.updateDetail = (req, res) => {
        console.log(req.body)
        if (!req.body) {
          res.status(400).send({
            message: "Content can not be empty!"
          });
        }


      
      var article_keys={
      job_bidding_job_bidding_id: req.body.job_bidding_job_bidding_id,
      job_posted_job_id:req.body.job_posted_job_id,
      article_details_publisher_id: req.body.article_details_publisher_id,
      }

        var article_update={
        
          article_details_publisher_article_wordcount: req.body.article_details_publisher_article_wordcount,
          article_details_publisher_headings: req.body.article_details_publisher_headings,
          article_details_publisher_images_number: req.body.article_details_publisher_images_number,
          article_details_publisher_plagirism_percentage: req.body.article_details_publisher_plagirism_percentage,
          article_details_publisher_category_id: req.body.article_details_publisher_category_id,
          article_details_publisher_article_status: req.body.article_details_publisher_article_status,
          article_details_publisher_article_suggested_title: req.body.article_details_publisher_article_suggested_title,
          article_details_publisher_seo_optimized: req.body.article_details_publisher_seo_optimized,
          article_details_article_description: req.body.article_details_article_description,
          article_details_publisher_article_source: req.body.article_details_publisher_article_source,
        }
  
    
        // Save Customer in the database
        Change.updateDetail(article_keys, article_update, (err, data) => {
          
          if (err){
         
              res.status(500).send({
                message:
                  err.message || "You Are Not Allowed to Update Article Detail."
              });
            } 
            
            else {
              res.status(200).send(
                {                
                status:"ok",
                message: "Article Detail Updated",    
                data,    
                }
            )      
            }
        });
      };
