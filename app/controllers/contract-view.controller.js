const UserBidView = require("../models/contract-view.model.js");





module.exports.contract_view_user=function(req,res){
   

    userjobId={
      "user_id":req.body.user_id
    }
  
    UserBidView.contract_view_user(userjobId, (err, data) => {
        if (err) {
          console.log(err)
            if (err =="Not Found") {
              res.status(404).send({
                message: `Not found Contract For This User`
              });
            } else {
              res.status(500).send({
                message: "Error retrieving Contract"
              });
            }
          } else {
            res.status(200).send(
              {                
              status:"ok",
              message: "Contract Posted",    
              data,    
              }
          )      
          }
                }
      
      );
    
  }


  