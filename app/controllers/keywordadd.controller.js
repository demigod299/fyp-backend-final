const ArticleDetails = require("../models/keyword-add.model.js");
const Status = require("../models/contractrecord.model.js");

exports.contract_step1 = (req, res) => {
  let values = [];
  var keywordsdetails = {
    keywords: [],
  };
  console.log(req.body.keywords);
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
  }

  if (req.body.keywords.length > 0) {
    var userdata = {
      bid_id: req.body.data.job_bidding_id,
      job_id: req.body.data.job_id,
      bid_user_id: req.body.data.article_details_publisher_id,
      article_details_publisher_id: req.body.data.article_details_publisher_id,
    };

    for (i = 0; i < req.body.keywords.length; i++) {
      keywordsdetails.keywords[i] = {
        keyword_name: req.body.keywords[i].keyword_name,
        keyword_type: req.body.keywords[i].keyword_type,
        keyword_length: req.body.keywords[i].keyword_length,
        keyword_deleted: 0,
        keyword_enabled: 1,
      };
      values.push(
        new Array(
          req.body.keywords[i].keyword_name,
          req.body.keywords[i].keyword_length,
          req.body.keywords[i].keyword_type,
          0,
          1,
          req.body.data.job_bidding_id,
          req.body.data.article_details_publisher_id
        )
      );
    }

    ArticleDetails.contract_step1(
      values,
      userdata,
      keywordsdetails,
      (err, data) => {
        if (err) {
          res.status(500).send({
            message: err.message || "You Have Already Placed Article Details.",
          });
        } else {
          res.status(200).send({
            status: "ok",
            message: "Article Placed",
            data,
          });
        }
      }
    );
  }
};

exports.viewkeywords_articles = (req, res) => {
  var contract_details = {
    article_details_publisher_article_details_publisher_id:
      req.body.article_details_publisher_article_details_publisher_id,
  };

  ArticleDetails.viewkeywords_articles(contract_details, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while retrieving Contract Record.",
      });
    else {
      res.status(200).send({
        status: "ok",
        message: "Contract Retrieved Successfully",
        data,
      });
    }
  });
};

exports.deletekeywords_articles = (req, res) => {
  var contract_details = {
    keywords_id: req.body.keywords_id,
    article_details_publisher_article_details_publisher_id:
      req.body.article_details_publisher_article_details_publisher_id,
  };

  ArticleDetails.deletekeywords_articles(contract_details, (err, data) => {
    if (err)
      if ((err = "")) {
        res.status(500).send({
          message: err.message || "Job Disabled.",
        });
      } else {
        res.status(500).send({
          message:
            err.message || "Some error occurred while Deleting keywords.",
        });
      }
    else {
      res.status(200).send({
        status: "ok",
        message: "keywords Deleted Successfully",
        data,
      });
    }
  });
};

exports.findAll = (req, res) => {
  var contract_details = {
    contract_details_steps_status: req.body.contract_details_steps_status,
    job_bidding_job_bidding_id: req.body.job_bidding_job_bidding_id,
  };

  Status.findAll(contract_details, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while retrieving Contract Record.",
      });
    else {
      res.status(200).send({
        status: "ok",
        message: "Contract Retrieved Successfully",
        data,
      });
    }
  });
};
