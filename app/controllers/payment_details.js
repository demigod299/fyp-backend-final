const Users = require("../models/payment_detail.mode.js");

// Retrieve all Customers from the database.
exports.addPayment = (req, res) => {
  var dataOj = {
    paymentMethod: req.body.paymentMethod,
    accountNumber: req.body.accountNumber,
    userId: req.body.userId,
  };

  Users.addPayment(dataOj, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving User.",
      });
    else res.send(data);
  });
};
exports.getPaymentDetail = (req, res) => {
  var dataOj = {
    userId: req.body.userId,
  };
  Users.getPaymentDetail(dataOj, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving Payment.",
      });
    else res.send(data);
  });
};
