const article = require("../models/admin.model");

exports.findAll = (req, res) => {
  article.getAll((err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving Users.",
      });
    else res.send(data);
  });
};

exports.UpdateUser = (req, res) => {
  var newUser = {
    id: req.body.id,
    status: req.body.status,
  };
  article.UpdateUser(newUser, (err, data) => {
    if (err)
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving Users.",
      });
    else {
      res.status(200).send({
        message: "Status Updated.",
      });
    }
  });
};
