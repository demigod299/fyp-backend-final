const UserBidView = require("../models/publisher-view-bids.model.js");

module.exports.findbidsbystatususerid = function (req, res) {
  userjobId = {
    status: req.body.job_bid_status,
    user_id: req.body.user_id,
  };

  UserBidView.findbidsbystatususerid(userjobId, (err, data) => {
    if (err) {
      console.log(err);
      if (err == "Not Found") {
        res.status(404).send({
          message: `Not found Bid For This User`,
        });
      } else {
        res.status(500).send({
          message: "Error retrieving Bids",
        });
      }
    } else {
      res.status(200).send({
        status: "ok",
        message: "Job Posted",
        data,
      });
    }
  });
};

module.exports.findbidsbystatususeridandjob_bid_id = function (req, res) {
  userjobId = {
    status: req.body.job_bid_status,
    user_id: req.body.user_id,
    bid_job_id: req.body.bid_job_id,
  };

  UserBidView.findbidsbystatususeridandjob_bid_id(userjobId, (err, data) => {
    if (err) {
      console.log(err);
      if (err == "Not Found") {
        res.status(404).send({
          message: `Not found Bid For This User`,
        });
      } else {
        res.status(500).send({
          message: "Error retrieving Bids",
        });
      }
    } else {
      res.status(200).send({
        status: "ok",
        message: "Job Posted",
        data,
      });
    }
  });
};

module.exports.updateArticleDetails = function (req, res) {
  var article_details = {
    article_id: req.body.article_id,
    article_status: req.body.article_status,
  };
  console.log(article_details);
  UserBidView.updateArticleDetails(article_details, (err, data) => {
    if (err) {
      res.status(500).send({
        message: "No Article Found",
      });
    } else {
      res.status(200).send({
        status: "ok",
        message: "Article Updated Retreieved Successfully",
        data,
      });
    }
  });
};
