const JobBidView = require("../models/writer-accepted-bids.model.js");





module.exports.findbidsbystatususerid=function(req,res){
   

    userjobId={
      "status": req.body.job_bid_status,
      "user_id":req.body.user_id
    }
  
    JobBidView.findbidsbystatususerid(userjobId, (err, data) => {
        if (err) {
          console.log(err)
            if (err =="Not Found") {
              res.status(404).send({
                message: `Not found Bid For This User`
              });
            } else {
              res.status(500).send({
                message: "Error retrieving Bids"
              });
            }
          } else {
            res.status(200).send(
              {                
              status:"ok",
              message: "Job Posted",    
              data,    
              }
          )      
          }
                }
      
      );
    
  }



  module.exports.contract_view_user=function(req,res){
   

    userjobId={
      "user_id":req.body.user_id
    }
  
    JobBidView.contract_view_user(userjobId, (err, data) => {
        if (err) {
          console.log(err)
            if (err =="Not Found") {
              res.status(404).send({
                message: `Not found Contract For This User`
              });
            } else {
              res.status(500).send({
                message: "Error retrieving Contract"
              });
            }
          } else {
            res.status(200).send(
              {                
              status:"ok",
              message: "Contract Posted",    
              data,    
              }
          )      
          }
                }
      
      );
    
  }


  module.exports.contract_accept_user=function(req,res){
   

    userjobId={
      "contract_id":req.body.contract_id
    }
  
    JobBidView.contract_accept_user(userjobId, (err, data) => {
        if (err) {
          console.log(err)
            if (err =="Not Found") {
              res.status(404).send({
                message: `Not found Contract For This User`
              });
            } else {
              res.status(500).send({
                message: "Error Enabling Contract"
              });
            }
          } else {
            res.status(200).send(
              {                
              status:"ok",
              message: "Contract Enabled",    
              data,    
              }
          )      
          }
                }
      
      );
    
  }
