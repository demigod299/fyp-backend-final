const User = require("../models/job.model.js");
var Cryptr = require('cryptr');

exports.create = (req, res) => {
    console.log(req.body)
    if (!req.body) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
    }
  
    const user = new User({
      job_title: req.body.job_title,
      job_description: req.body.job_description,
      price_job: req.body.price_job,
      words_job: req.body.words_job,
      number_articles: req.body.number_articles,
      days_to_finish: req.body.days_to_finish,
      user_id: req.body.user_id,
      job_type: req.body.job_type,
      job_pub_status: req.body.job_pub_status,

    });
  
    // Save Customer in the database
    User.create(user, (err, data) => {
      console.log(user)
      if (err){
     
          res.status(500).send({
            message:
              err.message || "You Are Not Allowed to Post."
          });
        } 
        
        else {
          res.status(200).send(
            {                
            status:"ok",
            message: "Job Posted",    
            data,    
            }
        )      
        }
    });
  };

  exports.findAll = (req, res) => {


    User.getAll((err, data) => {
        if (err)
          res.status(500).send({
            message:
              err.message || "Some error occurred while retrieving Country."
          });
          else {
            res.status(200).send(
              {                
              status:"ok",
              message: "Job Posted",    
              data,    
              }
          )      
          }
      });
    };


module.exports.findbyID=function(req,res){
      
      var userdata={
          "user_id": req.body.user_id,
          
      }
      User.findbyID(userdata, (err, data) => {
        console.log(userdata)
          if (err) {
              if (err === "not_found") {
                res.status(404).send({
                  message: `Not found Jobs`
                });
              } else {
                res.status(500).send({
                  message: "Error retrieving Jobs"
                });
              }
            } else {
              res.status(200).send(
                {                
                status:"ok",
                message: "Job Posted",    
                data,    
                }
            )      
            }
                  }
        
        );
      
  }
  

  module.exports.findbyIDandJobid=function(req,res){
      
    userjobId={
      "job_id": req.body.job_id,
      "user_id": req.body.user_id,
    }
    User.findbyIDandJobid(userjobId, (err, data) => {

      if (err) {
          console.log(err)
            if (err =="Not Found") {
              res.status(404).send({
                message: `Not found Jobs For This User`
              });
            } else {
              res.status(500).send({
                message: "Error retrieving Jobs"
              });
            }
          } else {
            res.status(200).send(
              {                
              status:"ok",
              message: "Job Posted",    
              data,    
              }
          )      
          }
                }
      
      );
    
}


module.exports.findbyIDandJobidWriter=function(req,res){
      
  userjobId={
    "job_id": req.body.job_id,
    
  }
  User.findbyIDandJobidWriter(userjobId, (err, data) => {
      if (err) {
        console.log(err)
          if (err =="Not Found") {
            res.status(404).send({
              message: `Not found Jobs`
            });
          } else {
            res.status(500).send({
              message: "Error retrieving Jobs"
            });
          }
        } else {
          res.status(200).send(
            {                
            status:"ok",
            message: "Job Posted",    
            data,    
            }
        )      
        }
              }
    
    );
  
}
