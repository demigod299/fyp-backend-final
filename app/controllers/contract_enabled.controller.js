const contract_enabled = require("../models/contract_enabled.model.js");





module.exports.contract_enable_user=function(req,res){
   

    userjobId={
      "contract_id":req.body.contract_id
    }
  
    contract_enabled.contract_enable_user(userjobId, (err, data) => {
        if (err) {
          console.log(err)
            if (err =="Not Found") {
              res.status(404).send({
                message: `Not found Contract For This User`
              });
            } else {
              res.status(500).send({
                message: "Error Enabling Contract"
              });
            }
          } else {
            res.status(200).send(
              {                
              status:"ok",
              message: "Contract Enabled",    
              data,    
              }
          )      
          }
                }
      
      );
    
  }


  