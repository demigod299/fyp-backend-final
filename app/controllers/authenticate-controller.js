const User = require("../models/authenticate.model.js");
const jwt = require("jsonwebtoken");

let secret = "myTotalySecretKey";
module.exports.authenticate = function (req, res) {
  const user = new User({
    email: req.body.email,
    password: req.body.password,
  });
  var userdata = {
    email: req.body.email,
    password: req.body.password,
  };
  let token = jwt.sign(userdata, secret, { expiresIn: "6000m" });
  User.authenticate(user, (err, data) => {
    console.log(err);
    if (err == "No Email/Username Found") {
      res.status(500).send({
        message: err.message || "No User Found With This Email.",
      });
    } else if (err == "Not Authenticated") {
      res.status(500).send({
        message: err.message || "Your Password Seems to Be Incorrect.",
      });
    } else {
      res.status(200).send({
        status: "ok",
        message: "User Authenticated",
        data,
        token: token,
      });
    }
  });
};
