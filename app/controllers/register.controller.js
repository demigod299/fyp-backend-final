const User = require("../models/register.model.js");
var Cryptr = require("cryptr");

exports.register = (req, res) => {
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
  }

  var encryptedString = cryptr.encrypt(req.body.password);
  const user = new User({
    email: req.body.email,
    username: req.body.username,
    password: encryptedString,
    name: req.body.name,
    phone: req.body.phone,
    type: req.body.type,
    city: req.body.city,
  });

  // Save Customer in the database
  User.registeruser(user, (err, data) => {
    if (err)
      if ((err = "Email or Username Already Exist")) {
        res.status(500).send({
          message: err.message || "Email or Username Already Exist.",
        });
      } else {
        res.status(500).send({
          message: err.message || "Error Creating User.",
        });
      }
    else res.send(data);
  });
};
