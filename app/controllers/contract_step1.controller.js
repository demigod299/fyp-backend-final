

const ArticleDetails = require("../models/contract-details-step1.model.js");
const Status = require("../models/contractrecord.model.js");



exports.contract_step1 = (req, res) => {
  let values = [];

    if (!req.body) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
    }
    articledetails_get={     
      job_bidding_job_bidding_id:req.body[0].job_bidding_job_bidding_id   
     }
    
  if(req.body.length>0){
    for(i=0;i<req.body.length;i++){    
    
       values.push(new Array(
        req.body[i].job_posted_job_id, 
        req.body[i].article_details_publisher_article_suggested_title, 
        req.body[i].article_details_publisher_seo_optimized, 
        req.body[i].article_details_publisher_article_wordcount, 
        req.body[i].article_details_article_description, 
        req.body[i].article_details_publisher_headings,         
        req.body[i].article_details_publisher_article_source,         
        req.body[i].article_details_publisher_images_number, 
        req.body[i].article_details_publisher_plagirism_percentage, 
        req.body[i].article_details_publisher_category_id,
        "Article Details Added", 
        0,
        0,
        "Article Placed",
        req.body[i].job_bidding_job_bidding_id
        ));
		}
    
      
      ArticleDetails.contract_step1(values,articledetails_get, (err, data) => {      

        if (err){
       
            res.status(500).send({
              message:
                err.message || "You Have Already Placed Article Details."
            });
          } 
          
          else {
            res.status(200).send(
              {                
              status:"ok",
              message: "Article Placed",    
              }
          )      
          }
            
      });
  


    

  }
  
  };


  exports.findAll = (req, res) => {

  var contract_details = {
    contract_details_steps_status: req.body.contract_details_steps_status,
    job_bidding_job_bidding_id: req.body.job_bidding_job_bidding_id
  }
 
  Status.findAll(contract_details, (err, data) => {
        if (err)
          res.status(500).send({
            message:
              err.message || "Some error occurred while retrieving Contract Record."
          });
          else {
            res.status(200).send(
              {                
              status:"ok",
              message: "Contract Retrieved Successfully",    
              data,    
              }
          )      
          }
      });
    };


    